# Trivia Game

Trivia Game assignment in Vue.js. 

Do note that there is a sporadic error when logging in with a new user. 
By choosing category and clicking continue the app works again.

## Component tree
![Component tree](./Component tree.png "Title")

## Installation
``npm install``

## Usage
``npm run dev``

## Authors and acknowledgment
@mikaelb
@sigurd12345

## License
MIT

