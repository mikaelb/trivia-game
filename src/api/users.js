const BASE_URL = "https://mb-noroff-api.herokuapp.com"
const API_KEY = "k+2BizNBeEO3ETi7L4L7fQ=="

// TODO: API call for fetching all users (might not be necessary, depending on how apiCheckUsername ends)

export async function apiCheckUsername(username) {
    try {
        const response = await fetch(`${BASE_URL}/trivia?username=${username}`)

        if (!response.ok) {
            throw new Error('Could not check for user')
        }

        const data = await response.json()

        return [null, data]
    }
    catch (error) {
        return [error.message, null]
    }
}

export async function apiUserRegistration(username) {
    try {
        const config = {
            method: 'POST',
            headers: {
                'X-API-Key': API_KEY,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                highScore: 0
            })
        }
        const response = await fetch(`${BASE_URL}/trivia`, config)

        if (!response.ok) {
            throw new Error('Could not create new user')
        }

        const data = await response.json()

        return [null, data]
    }
    catch (error) {
        return [error.message, null]
    }
}

export async function apiUpdateUserHighScore(userId, newHighScore) {
    try {


        const config = {
            method: 'PATCH',
            headers: {
                'X-API-Key': API_KEY,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                highScore: newHighScore
            })
        }

        const response = await fetch(`${BASE_URL}/trivia/${userId}`, config)

        if (!response.ok) {
            throw new Error('Could not update high score')
        }

        const data = await response.json()

        return [null, data]

    } catch (error) {
        return [error.message, null]
    }
}

export async function apiGetUserHighscore(username){
    try {
        const response = await fetch(`${BASE_URL}/trivia?username=${username}`)

        if (!response.ok) {
            throw new Error('Could not check for user')
        }

        const data = await response.json()
        let value = "";
        return [null, data[0].highScore];
    }
    catch (error) {
        return [error.message, 0]
    }
}