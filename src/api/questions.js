const BASE_URL = "https://opentdb.com"

export async function apiFetchQuestionCategories() {
    try {
        const response = await fetch(`${BASE_URL}/api_category.php`)

        if (!response.ok) {
            throw new Error('Could not fetch question categories')
        }

        const data = await response.json()

        return [null, data.trivia_categories]
    }
    catch (error) {
        return [error.message, null]
    }
}

export async function apiFetchQuestions(amount, category, difficulty, type, token) {
    try {
        let apiUrl = `${BASE_URL}/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}`;
        if (type){
            apiUrl+=`&type=${type}`
        }
        if (token){
            apiUrl += `&token=${token}`;
        }
        console.log(apiUrl)
        const response = await fetch(apiUrl)

        if (!response.ok) {
            throw new Error('Could not fetch questions')
        }

        const data = await response.json()
        return [null, data.results]
    }
    catch (error) {
        return [error.message, null]
    }
}

// TODO: Add API call for fetching session token
export async function apiFetchSessionToken() {
    try {
        const response = await fetch(`${BASE_URL}/api_token.php?command=request`)

        if (!response.ok) {
            throw new Error('Could not fetch session token')
        }

        const data = await response.json()

        return [null, data]
    } catch (error) {
        return [error.message, null]
    }
}