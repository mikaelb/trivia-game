import { createStore } from 'vuex';
import { ref } from 'vue';
import { apiFetchQuestionCategories, apiFetchQuestions, apiFetchSessionToken } from './api/questions';
import { apiUpdateUserHighScore, apiUserRegistration, apiCheckUsername } from './api/users'

const initUser = () => {
    const storedUser = localStorage.getItem("trivia-user")
    if (!storedUser) {
        return null
    }

    return JSON.parse(storedUser)
}

const initSessionToken = () => {
    const storedSession = localStorage.getItem("session-token")
    if (!storedSession) {
        return null
    }

    return JSON.parse(storedSession)
}

// Stores categories incase of refresh (not really needed)
const initCategories = () => {
    const storedCategories = localStorage.getItem("question-categories")
    if (!storedCategories) {
        return []
    }

    return JSON.parse(storedCategories)
}

// Stores current questions
const initQuestions = () => {
    const storedQuestions = localStorage.getItem("questions")
    if (!storedQuestions) {
        return []
    }

    return JSON.parse(storedQuestions)
}

const initUserAnswers = () => {
    const storedAnswers = localStorage.getItem("user-answers")
    if (!storedAnswers) {
        return []
    }

    return JSON.parse(storedAnswers)
}

const initCurrentQuestion = () => {
    const storedCurrentQuestion = localStorage.getItem("current-question")
    if (!storedCurrentQuestion) {
        return 0
    }

    return JSON.parse(storedCurrentQuestion)
}

const initCurrentScore = () => {
    const storedScore = localStorage.getItem("current-score")
    if (!storedScore) {
        return 0
    }

    return JSON.parse(storedScore)
}

const initCurrentQuestionParameters = () => {
    const storedQuestionParameters = localStorage.getItem("question-parameters")
    if (!storedQuestionParameters) {
        return {}
    }

    return JSON.parse(storedQuestionParameters)
}

export default createStore({
    state: {
        user: initUser(),
        sessionToken: initSessionToken(),
        questionCategories: initCategories(),
        questions: initQuestions(),
        userAnswers: initUserAnswers(),
        currentQuestion: initCurrentQuestion(),
        currentScore: initCurrentScore(),
        currentQuestionParameters: initCurrentQuestionParameters()
    },
    getters: {
        
    },
    mutations: {
        
        setUser: (state, user) => {
            state.user = user
        },
        setHighScore: (state, highScore) => {
            state.highScore = highScore
        },
        setCategories: (state, categories) => {
            state.questionCategories = categories
        },
        setQuestions: (state, questions) => {
            state.questions = questions
        },
        setSessionToken: (state, sessionToken) => {
            state.sessionToken = sessionToken
        },
        addToUserAnswers: (state, answer) => {
            state.userAnswers.push(answer)
        },
        resetUserAnswers: (state) => {
            state.userAnswers = []
        },
        increaseCurrentQuestion: (state) => {
            state.currentQuestion += 1
        },
        resetCurrentQuestion: (state) => {
            state.currentQuestion = 0
        },
        increaseCurrentScore: (state) => {
            state.currentScore += 10
        },
        resetCurrentScore: (state) => {
            state.currentScore = 0
        },
        setCurrentQuestionParameters: (state, parameters) => {
            state.currentQuestionParameters = parameters
        }
    },
    actions: {
        // Checks if user is already in "database", if not registers new user object. 
        // Before assigning user to state
        async registerLogin({ commit }, { username }) {
            try {
                let user = {}
                let [error, users] = await apiCheckUsername(
                    username.value
                )

                if (error !== null) {
                    throw new Error(error)
                }

                if (users.length > 0) {
                    console.log("re-using username: " + users[0])
                    user = users[0]
                } else {
                    [error, user] = await apiUserRegistration(
                        username.value
                    )

                    if (error !== null) {
                        throw new Error(error)
                    }
                }
                commit("setUser", user)

                localStorage.setItem("trivia-user", JSON.stringify(user))

                return null
            }
            catch (e) {
                return e.message
            }
        },
        
        async updateHighScore({ commit }, { userId, highScore }) {
            try {
                if(state.currentScore > state.highScore) {
                    const [error, user] = await apiUpdateUserHighScore(
                        userId,
                        state.currentScore
                    )
    
                    if (error !== null) {
                        throw new Error(error)
                    }
    
                    commit("setHighScore", state.currentScore)

                    return true
                }

                return false
            }
            catch (e) {
                return e.message
            }
        },
        
        async checkUsername({ }, { username }) {
            try {
                const [error, user] = await apiCheckUsername(
                    username.value
                )

                if (error !== null) {
                    throw new Error(error)
                }

                return null
            }
            catch (e) {
                return e.message
            }
        },
        async fetchQuestionCategories({ commit }) {
            try {
                const [error, categories] = await apiFetchQuestionCategories()

                if (error !== null) {
                    throw new Error(error)
                }

                commit("setCategories", categories)

                localStorage.setItem("question-categories", JSON.stringify(categories))

                return null
            }
            catch (e) {
                return e.message
            }
        },
        
        async fetchQuestions({ commit, state }, { amount, category, difficulty, type }) {
            try {
                const [error, questions] = await apiFetchQuestions(
                    amount.value,
                    category.value,
                    difficulty.value,
                    type.value,
                    state.sessionToken
                )

                if (error !== null) {
                    throw new Error(error)
                }

                commit("setCurrentQuestionParameters", { amount, category, difficulty, type })
                commit("setQuestions", questions)
                commit("resetCurrentQuestion")
                commit("resetUserAnswers")
                commit("resetCurrentScore")

                localStorage.setItem("question-parameters", JSON.stringify(state.currentQuestionParameters))
                localStorage.setItem("user-answers", JSON.stringify(state.userAnswers))
                localStorage.setItem("current-question", JSON.stringify(state.currentQuestion))
                localStorage.setItem("questions", JSON.stringify(questions))

                return null
            }
            catch (e) {
                return e.message
            }
        },
        async fetchSessionToken({ commit }) {
            try {
                const [error, sessionToken] = await apiFetchSessionToken()

                if (error !== null) {
                    throw new Error(error)
                }

                commit("setSessionToken", sessionToken.token)

                localStorage.setItem("session-token", JSON.stringify(sessionToken.token))

                return null
            }
            catch (e) {
                return e.message
            }
        },
        async addAnswerToUserAnswers({ commit, state }, answer) {
            if (answer === state.questions[state.currentQuestion].correct_answer) {
                commit("increaseCurrentScore")
            }

            commit("addToUserAnswers", answer)
            commit("increaseCurrentQuestion")

            localStorage.setItem("user-answers", JSON.stringify(state.userAnswers))
            localStorage.setItem("current-question", JSON.stringify(state.currentQuestion))

            return null
        }
    }
})