import { createRouter, createWebHistory } from 'vue-router';
import store from './store'
import Start from './views/Start.vue'
import Questions from './views/Questions.vue'
import GetReady from './views/GetReady.vue'
import Results from './views/Results.vue'

const authGuard = (to, from, next) => {
    if (!store.state.user) {
        next("/");
    } else {
        next();
    }
}
const routes = [
    {
        name: "Start",
        path: "/",
        component: Start
    },
    {
        name: "Questions",
        path: "/questions",
        component: Questions,
        beforeEnter: authGuard
    },
    {
        name: "Results",
        path: "/results",
        component: Results,
        beforeEnter: authGuard
    },
    {
        name: "GetReady",
        path: "/getready",
        component: GetReady,
        beforeEnter: authGuard
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})
